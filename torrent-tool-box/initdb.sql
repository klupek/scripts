create table releases( id INT, name varchar(255), category varchar(32), size bigint, genre varchar(255), more varchar(255), time TIMESTAMP );
create table search_results(release_id INT, id INT);
create table ignores ( id INTEGER PRIMARY KEY AUTOINCREMENT, name varchar(255), category varchar(32), genre varchar(255) );
create table likes ( id INTEGER PRIMARY KEY AUTOINCREMENT, name varchar(255), category varchar(32), genre varchar(255), since varchar(255) );
create table downloaded ( release_id INT, time TIMESTAMP );
create table irc_files( filename varchar(255), position INT default '0' );
create table config ( name varchar(100), val varchar(255) );
create table subtitles ( release_name varchar(255), subtitle_url varchar(255));
